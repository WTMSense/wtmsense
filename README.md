```
#!python

import RPi.GPIO as GPIO
import datetime
import time
import pytz

import requests
from time import sleep
from datetime import datetime
from Adafruit_CharLCD import Adafruit_CharLCD
lcd = Adafruit_CharLCD(rs=25, en=24, d4=23, d5=17, d6=21, d7=22, cols=16, lines=2)
lcd.clear()
flag =1

cities =  [('St. Louis', '2486982' , 'US/St. Louis'), ('St. Louis', '2486982' , 'US/St. Louis'), ('St. Louis', '2486982' , 'US/St. Louis'), ('St. Louis', '2486982' , 'US/St. Louis'), ('St. Louis', '2486982' , 'US/St. Louis'), ('St. Louis', '2486982' , 'US/St. Louis')]

city_codes = ",".join([c[1] for c in cities])

def getWeatherConditions() :
    try :
        url = 'https://query.yahooapis.com/v1/public/yql'
        # encode query string for request
        query = ('select item.condition from weather.forecast where woeid in ('
                + city_codes + ')')
        query_strings = {'q': query, 'format' : 'json'}
        # headers to disable caching (in theory)
        headers = {'Pragma': 'no-cache', 'Cache-Control': 'no-cache'}
        # get weather data from Yahoo
        r = requests.get(url, params=query_strings, headers=headers)
        # return data formatted to JSON
        return r.json()
    except Exception:
        return "Feed Error"
    

if __name__ == "__main__":
    while 1:
        data = getWeatherConditions()
        if( type(data) != type(dict()) or "error" in data):
            # Error
            print (data)
            lcd.clear()
            lcd.message("Connecting\nto Wifi")
            #lcd.clear()
            #sleep(4)
        else:
            count = data["query"]["count"]
            # Loop through cities
            for c in range(0, count):
                # Retrieve city name, condition and temperature
                city = cities[c][0]
                cond = data["query"]["results"]["channel"][c]["item"]["condition"]["text"]
                temp = data["query"]["results"]["channel"][c]["item"]["condition"]["temp"]
               
            
            GPIO.setmode(GPIO.BCM)
            
            GPIO.setup(25, GPIO.OUT)
            GPIO.setup(24, GPIO.OUT)
            GPIO.setup(23, GPIO.OUT)
            GPIO.setup(17, GPIO.OUT)
            GPIO.setup(21, GPIO.OUT)
            GPIO.setup(22, GPIO.OUT)
            

            TRIG1=10
            ECHO1=9
            TRIG2=16
            ECHO2=20
            counter = 0
            avgdist1 = 0
            avgdist2 = 0
            
            while counter < 4:
                #distance measurement
                GPIO.setup(TRIG1, GPIO.OUT)
                GPIO.output(TRIG1, False)

                GPIO.setup(ECHO1, GPIO.IN)

                GPIO.setup(TRIG2, GPIO.OUT)
                GPIO.output(TRIG2, False)
                
                GPIO.setup(ECHO2, GPIO.IN)

                #waiting for sensor 1 to settle
                time.sleep(.1)
                GPIO.output(TRIG1, True)
                time.sleep(0.00001)
                GPIO.output(TRIG1, False)

                while GPIO.input(ECHO1) == 0:
                    pass
                    pulse_start1 = time.time()
                 
                while GPIO.input(ECHO1) == 1:
                    pass
                    pulse_end1 = time.time()

                pulse_duration1 = pulse_end1 - pulse_start1

                distance1 = pulse_duration1 * 17150
                distance1= round(distance1, 2)

                #waiting for sensor 2 to settle
                time.sleep(.1)
                GPIO.output(TRIG2, True)
                time.sleep(0.00001)
                GPIO.output(TRIG2, False)
                
                while GPIO.input(ECHO2) == 0:
                    pass
                    pulse_start2 = time.time()
                 
                while GPIO.input(ECHO2) == 1:
                    pass
                    pulse_end2 = time.time()
                    
                pulse_duration2 = pulse_end2 - pulse_start2
                
                distance2 = pulse_duration2 * 17150
                distance2 = round(distance2, 2)

                avgdist1 = avgdist1 + distance1
                avgdist2 = avgdist2 + distance2
                
                counter = counter +1

                #blue distance1 sensor = left sensor
                #red distance2 sensor = right sensor

            avgdist1 = avgdist1/4
            #print ("sensor 1", avgdist1)
            avgdist2 = avgdist2/4
            #print ("sensor 2", avgdist2)


            
            if (avgdist2<15 and avgdist1>15):
                flag=0
            if (avgdist1<15):
                flag=1
            if (flag==1):
                lcd.clear()
                lcd.message(datetime.now().strftime('%I:%M %p %b %d'))
            if (flag==0):
                lcd.clear()
                lcd.message(city + " " + temp)
                lcd.write8(223, True)
                lcd.message("F\n" + cond)
            
            GPIO.cleanup()


```